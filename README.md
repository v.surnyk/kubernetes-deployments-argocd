1. kubectl --namespace argocd apply -filename https://raw.githubusercontent.com/argoproj-labs/argocd-image-updater/stable/manifests/install.yaml

2. kubectl --namespace argocd logs --selector app.kubernetes.io/name=argocd-image-updater

3. kubectl --namespace argocd logs \
    --selector app.kubernetes.io/name=argocd-image-updater \
    --follow


5. kubectl port-forward -n argocd svc/argocd-server 8889:443
Get a password : ``` kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo```





